﻿using Entitas;
using IdleMine.Simulation;
using IdleMine.Utilities;
using NUnit.Framework;
using System.Linq;

namespace IdleMine.Simulation.Tests
{
    [TestFixture]
    public class CreateMineWhenIsGameStartTest
    {
        Contexts contexts;
        IExecuteSystem system;

        [SetUp]
        public void SetUp()
        {
            this.contexts = new Contexts();
            this.system = new CreateMineRoomSystem(contexts);
        }

        [TearDown]
        public void TearDown()
        {
            this.contexts = null;
            this.system = null;
        }

        [Test]
        public void MineIsCreated()
        {
            var startGame = contexts.simulation.CreateEntity().isStartGame = true;
            system.Execute();
            var isMineCreated = contexts.simulation.GetEntities(SimulationMatcher.MineRoom).Any();

            Assert.IsTrue(isMineCreated);
        }


    }
}
