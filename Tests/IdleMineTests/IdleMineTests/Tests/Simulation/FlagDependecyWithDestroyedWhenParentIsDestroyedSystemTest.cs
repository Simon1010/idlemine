﻿using Entitas;
using NUnit.Framework;

namespace IdleMine.Simulation.Tests
{
    [TestFixture]
    public class FlagDependecyWithDestroyedWhenParentIsDestroyedSystemTest
    {
        private Contexts contexts;
        private IInitializeSystem idSystem;
        private IExecuteSystem system;

        [SetUp]
        public void SetUp()
        {
            this.contexts = Contexts.sharedInstance;
            this.system = new FlagDependencyWithDestroyedWhenParentIsDestroyedSystem(contexts);
            this.idSystem = new AssignIdSystem(contexts);
            idSystem.Initialize();
        }

        [TearDown]
        public void TearDown()
        {
            this.contexts = null;
            this.system = null;
            this.idSystem = null;
        }

        [Test]
        public void DependenciesDestroyedCorrectly()
        {
            var a = contexts.simulation.CreateEntity();
            var b = contexts.simulation.CreateEntity();

            b.AddDependency(a.simulationId.value);
            a.isDestroyed = true;

            var c = contexts.simulation.CreateEntity();
            var d = contexts.simulation.CreateEntity();

            c.AddDependency(d.simulationId.value);
            d.AddDependency(c.simulationId.value);
            c.isDestroyed = true;

            var e = contexts.simulation.CreateEntity();
            var f = contexts.simulation.CreateEntity();
            e.AddDependency(f.simulationId.value);
            e.isDestroyed = true;

            system.Execute();

            Assert.IsTrue(b.isDestroyed);
            Assert.IsTrue(d.isDestroyed);
            Assert.IsTrue(f.isDestroyed == false);
        }


    }
}
