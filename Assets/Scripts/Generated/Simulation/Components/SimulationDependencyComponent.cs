//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class SimulationEntity {

    public IdleMine.Simulation.DependencyComponent dependency { get { return (IdleMine.Simulation.DependencyComponent)GetComponent(SimulationComponentsLookup.Dependency); } }
    public bool hasDependency { get { return HasComponent(SimulationComponentsLookup.Dependency); } }

    public void AddDependency(IdleMine.Utilities.SimulationEntityId newValue) {
        var index = SimulationComponentsLookup.Dependency;
        var component = (IdleMine.Simulation.DependencyComponent)CreateComponent(index, typeof(IdleMine.Simulation.DependencyComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceDependency(IdleMine.Utilities.SimulationEntityId newValue) {
        var index = SimulationComponentsLookup.Dependency;
        var component = (IdleMine.Simulation.DependencyComponent)CreateComponent(index, typeof(IdleMine.Simulation.DependencyComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveDependency() {
        RemoveComponent(SimulationComponentsLookup.Dependency);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class SimulationMatcher {

    static Entitas.IMatcher<SimulationEntity> _matcherDependency;

    public static Entitas.IMatcher<SimulationEntity> Dependency {
        get {
            if (_matcherDependency == null) {
                var matcher = (Entitas.Matcher<SimulationEntity>)Entitas.Matcher<SimulationEntity>.AllOf(SimulationComponentsLookup.Dependency);
                matcher.componentNames = SimulationComponentsLookup.componentNames;
                _matcherDependency = matcher;
            }

            return _matcherDependency;
        }
    }
}
