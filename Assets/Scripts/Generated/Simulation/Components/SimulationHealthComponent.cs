//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class SimulationEntity {

    public IdleMine.Simulation.HealthComponent health { get { return (IdleMine.Simulation.HealthComponent)GetComponent(SimulationComponentsLookup.Health); } }
    public bool hasHealth { get { return HasComponent(SimulationComponentsLookup.Health); } }

    public void AddHealth(float newValue) {
        var index = SimulationComponentsLookup.Health;
        var component = (IdleMine.Simulation.HealthComponent)CreateComponent(index, typeof(IdleMine.Simulation.HealthComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceHealth(float newValue) {
        var index = SimulationComponentsLookup.Health;
        var component = (IdleMine.Simulation.HealthComponent)CreateComponent(index, typeof(IdleMine.Simulation.HealthComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveHealth() {
        RemoveComponent(SimulationComponentsLookup.Health);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class SimulationMatcher {

    static Entitas.IMatcher<SimulationEntity> _matcherHealth;

    public static Entitas.IMatcher<SimulationEntity> Health {
        get {
            if (_matcherHealth == null) {
                var matcher = (Entitas.Matcher<SimulationEntity>)Entitas.Matcher<SimulationEntity>.AllOf(SimulationComponentsLookup.Health);
                matcher.componentNames = SimulationComponentsLookup.componentNames;
                _matcherHealth = matcher;
            }

            return _matcherHealth;
        }
    }
}
