//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class ViewEntity {

    public IdleMine.View.ViewIdComponent viewId { get { return (IdleMine.View.ViewIdComponent)GetComponent(ViewComponentsLookup.ViewId); } }
    public bool hasViewId { get { return HasComponent(ViewComponentsLookup.ViewId); } }

    public void AddViewId(IdleMine.Utilities.ViewEntityId newValue) {
        var index = ViewComponentsLookup.ViewId;
        var component = (IdleMine.View.ViewIdComponent)CreateComponent(index, typeof(IdleMine.View.ViewIdComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceViewId(IdleMine.Utilities.ViewEntityId newValue) {
        var index = ViewComponentsLookup.ViewId;
        var component = (IdleMine.View.ViewIdComponent)CreateComponent(index, typeof(IdleMine.View.ViewIdComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveViewId() {
        RemoveComponent(ViewComponentsLookup.ViewId);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class ViewMatcher {

    static Entitas.IMatcher<ViewEntity> _matcherViewId;

    public static Entitas.IMatcher<ViewEntity> ViewId {
        get {
            if (_matcherViewId == null) {
                var matcher = (Entitas.Matcher<ViewEntity>)Entitas.Matcher<ViewEntity>.AllOf(ViewComponentsLookup.ViewId);
                matcher.componentNames = ViewComponentsLookup.componentNames;
                _matcherViewId = matcher;
            }

            return _matcherViewId;
        }
    }
}
