//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentLookupGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public static class ViewComponentsLookup {

    public const int AnimationNameListener = 0;
    public const int AnimationComplete = 1;
    public const int AnimationName = 2;
    public const int MineView = 3;
    public const int Parent = 4;
    public const int Transform = 5;
    public const int ViewId = 6;
    public const int ViewTarget = 7;
    public const int ParentListener = 8;

    public const int TotalComponents = 9;

    public static readonly string[] componentNames = {
        "AnimationNameListener",
        "AnimationComplete",
        "AnimationName",
        "MineView",
        "Parent",
        "Transform",
        "ViewId",
        "ViewTarget",
        "ParentListener"
    };

    public static readonly System.Type[] componentTypes = {
        typeof(AnimationNameListenerComponent),
        typeof(IdleMine.View.AnimationCompleteComponent),
        typeof(IdleMine.View.AnimationNameComponent),
        typeof(IdleMine.View.MineViewComponent),
        typeof(IdleMine.View.ParentComponent),
        typeof(IdleMine.View.TransformComponent),
        typeof(IdleMine.View.ViewIdComponent),
        typeof(IdleMine.View.ViewTargetComponent),
        typeof(ParentListenerComponent)
    };
}
