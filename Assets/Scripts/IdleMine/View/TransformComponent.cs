﻿using Entitas;

namespace IdleMine.View
{
    [View]
    public class TransformComponent : IComponent
    {
        public UnityEngine.Transform value;
    }
}
