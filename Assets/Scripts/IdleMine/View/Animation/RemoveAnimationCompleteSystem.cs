﻿using System.Collections.Generic;
using Entitas;

namespace IdleMine.View
{
    public class RemoveAnimationCompleteSystem : ReactiveSystem<ViewEntity>
    {
        private readonly Contexts contexts;

        public RemoveAnimationCompleteSystem(Contexts contexts) : base(contexts.view)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<ViewEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.isAnimationComplete = false;
            }
        }

        protected override bool Filter(ViewEntity entity)
        {
            return entity.isAnimationComplete;
        }

        protected override ICollector<ViewEntity> GetTrigger(IContext<ViewEntity> context)
        {
            return context.CreateCollector(ViewMatcher.AnimationComplete);
        }
    }
}
