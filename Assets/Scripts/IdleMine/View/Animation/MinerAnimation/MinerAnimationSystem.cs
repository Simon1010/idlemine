﻿using Entitas;
using IdleMine.Unity;
using System.Linq;

namespace IdleMine.View
{
    public class MinerAnimationSystem : IExecuteSystem
    {
        private readonly Contexts contexts;
        private IGroup<SimulationEntity> minerGroup;
        public MinerAnimationSystem(Contexts contexts)
        {
            this.contexts = contexts;
            minerGroup = contexts.simulation.GetGroup(SimulationMatcher.AllOf(SimulationMatcher.Character, SimulationMatcher.CharacterRole));
        }

        public void Execute()
        {
            var miners = minerGroup.GetEntities().Where(e => e.characterRole.value == Simulation.CharacterRole.Miner && e.aiBehaviour.value == Simulation.AiBehaviour.Mine);
            foreach (var miner in miners)
            {
                var minerViews = contexts.view.GetEntitiesWithViewTarget(miner.simulationId.value);
                foreach (var minerView in minerViews)
                {
                    if (miner.hasAiState)
                    {
                        if (miner.aiState.value == Simulation.AiState.Mining)
                        {
                            if (miner.hasMiningPreviousTime)
                            {
                                //TODO: Frame independent anims
                                var miningAttackFrame = minerView.transform.value.GetComponent<CharacterMinerView>().MiningAttackFrame / 60f;
                                var isMining = (UnityEngine.Time.time - (miner.miningPreviousTime.value - miningAttackFrame) >= miner.miningCooldown.value);

                                if (isMining)
                                {
                                    PlayAnim(minerView, "Mine");
                                }
                                else
                                {
                                    PlayAnim(minerView, "Idle", true);
                                }
                               
                            }
                        }
                    }
                }
            }
        }

        private void PlayAnim(ViewEntity minerView, string animName, bool waitTillCurrentFinished = false)
        {
            if (minerView.hasAnimationName == false || minerView.animationName.value != animName)
            {
                if (waitTillCurrentFinished == false || minerView.isAnimationComplete)
                {
                    minerView.ReplaceAnimationName(animName);
                }
            }
        }
    }
}
