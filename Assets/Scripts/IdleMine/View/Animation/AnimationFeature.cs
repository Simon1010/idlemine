﻿using Entitas;
namespace IdleMine.View
{
    public class AnimationFeature : Feature
    {
        public AnimationFeature(Contexts contexts)
        {
            Add(new MinerAnimationSystem(contexts));
            Add(new RemoveAnimationCompleteSystem(contexts));
        }
    }
}
