﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace IdleMine.View
{
    [View, Event(EventTarget.Self)]
    public class AnimationNameComponent : IComponent
    {
        public string value;
    }
}
