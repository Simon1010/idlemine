﻿namespace IdleMine.View
{
    public class ViewFeature : Feature
    {
        public ViewFeature(Contexts contexts)
        {
            Add(new AssignIdSystem(contexts));
            Add(new ParentFeature(contexts));
            Add(new AnimationFeature(contexts));


            Add(new ViewEventSystems(contexts));
        }
    }
}
