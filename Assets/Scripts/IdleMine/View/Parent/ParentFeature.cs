﻿using IdleMine.View;

namespace IdleMine
{
    public class ParentFeature : Feature
    {
        public ParentFeature(Contexts contexts)
        {
            Add(new SetParentWhenInMineSystem(contexts));
            Add(new SetParentWhenOnSurface(contexts));
        }
    }
}
