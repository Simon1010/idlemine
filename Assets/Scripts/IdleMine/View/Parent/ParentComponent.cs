﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using IdleMine.Utilities;
using UnityEngine;

namespace IdleMine.View
{
    [View, Event(EventTarget.Self)]
    public class ParentComponent : IComponent
    {
        public Transform value;
    }
}
