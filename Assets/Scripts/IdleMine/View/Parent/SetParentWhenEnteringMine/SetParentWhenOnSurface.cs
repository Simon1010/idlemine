﻿using System.Linq;
using Entitas;
using IdleMine.Unity;

namespace IdleMine.View
{
    public class SetParentWhenOnSurface : IExecuteSystem
    {
        private readonly Contexts contexts;

        public SetParentWhenOnSurface(Contexts contexts)
        {
            this.contexts = contexts;
        }


        public void Execute()
        {
            var entitiesOnSurface = contexts.simulation.GetEntities(SimulationMatcher.AnyOf(SimulationMatcher.Cart, SimulationMatcher.Character).NoneOf(SimulationMatcher.MineRoomId));


            foreach (var entity in entitiesOnSurface)
            {
                var entityViews = contexts.view.GetEntitiesWithViewTarget(entity.simulationId.value);

                if (entityViews.Any())
                {

                    var surfaceViewEntity = contexts.view.GetEntitiesWithViewTarget(contexts.simulation.surfaceEntity.simulationId.value).SingleOrDefault();

                    if (surfaceViewEntity != null)
                    {
                        foreach (var entityView in entityViews)
                        {
                            var surfaceView = surfaceViewEntity.transform.value.GetComponent<SurfaceView>();
                            entityView.ReplaceParent(surfaceView.EntityContainer);
                        }
                    }
                }
            }

        }
    }
}
