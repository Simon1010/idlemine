﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using IdleMine.Unity;

namespace IdleMine.View
{
    public class SetParentWhenInMineSystem : IExecuteSystem
    {
        private readonly Contexts contexts;

        public SetParentWhenInMineSystem(Contexts contexts)
        {
            this.contexts = contexts;
        }


        public void Execute()
        {
            var entitiesInMine = contexts.simulation.GetEntities(SimulationMatcher.MineRoomId).Where(e => e.isMineRoom == false);


            foreach (var entityInMine in entitiesInMine)
            {
                var entityViews = contexts.view.GetEntitiesWithViewTarget(entityInMine.simulationId.value);

                if (entityViews.Any())
                {

                    var mineRoom = contexts.simulation.GetEntitiesWithMineRoomId(entityInMine.mineRoomId.value).Single(e => e.isMineRoom);

                    if (mineRoom != null)
                    {
                        var mineRoomViewEntity = contexts.view.GetEntitiesWithViewTarget(mineRoom.simulationId.value).SingleOrDefault();

                        if (mineRoomViewEntity != null)
                        {
                            foreach (var entityView in entityViews)
                            {
                                var mineRoomView = mineRoomViewEntity.transform.value.GetComponent<MineRoomExcavatedView>();
                                entityView.ReplaceParent(mineRoomView.GetEntityContainer());
                            }
                        }
                    }
                }
            }

        }
    }
}
