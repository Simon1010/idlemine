﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using IdleMine.Utilities;

namespace IdleMine.View
{
    [View]
    public class ViewTargetComponent : IComponent
    {
        [EntityIndex] public SimulationEntityId value;
    }
}
