﻿using Entitas;

namespace IdleMine.View
{
    public class AssignIdSystem : IInitializeSystem
    {
        private readonly Contexts contexts;

        public AssignIdSystem(Contexts contexts)
        {
            this.contexts = contexts;
        }

        public void Initialize()
        {
            contexts.view.OnEntityCreated += AddId;
        }

        private void AddId(IContext context, IEntity entity)
        {
            var viewEntity = entity as ViewEntity;
            viewEntity.ReplaceViewId(new Utilities.ViewEntityId(entity.creationIndex));
        }
    }
}
