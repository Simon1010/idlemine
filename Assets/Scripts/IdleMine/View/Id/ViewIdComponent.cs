﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.View
{
    [View]
    public class ViewIdComponent : IComponent
    {
        [Entitas.CodeGeneration.Attributes.PrimaryEntityIndex] public ViewEntityId value;
    }
}
