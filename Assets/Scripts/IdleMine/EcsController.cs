﻿using Entitas;

namespace IdleMine
{
    public class EcsController
    {
        private Systems systems;
        public Contexts Contexts { get; private set; }

        public EcsController()
        {
            Contexts = Contexts.sharedInstance;
        }

        public void Initialize(Systems systems)
        {
            this.systems = systems;
            systems.Initialize();
        }

        public void Update()
        {
            systems.Execute();
        }
    
    }
}
