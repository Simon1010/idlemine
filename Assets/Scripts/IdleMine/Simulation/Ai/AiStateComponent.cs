﻿using Entitas;

namespace IdleMine.Simulation
{
    [Simulation]
    public class AiStateComponent : IComponent
    {
        public AiState value;
    }

    public enum AiState
    {
        Idle,
        Waiting,
        Mining,
        Storing,
        Collecting,
        MovingToMiningPoint,
        MovingToWaitingPoint,
    }
}
