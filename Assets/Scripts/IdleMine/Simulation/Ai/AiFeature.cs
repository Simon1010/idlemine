﻿namespace IdleMine.Simulation
{
    public class AiFeature : Feature
    {
        public AiFeature(Contexts contexts)
        {
            Add(new MinerBehaviourFeature(contexts));
            Add(new CharacterBehaviourFeature(contexts));
        }
    }
}
