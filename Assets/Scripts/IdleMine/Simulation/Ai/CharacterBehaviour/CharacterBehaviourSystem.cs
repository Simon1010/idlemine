﻿using Entitas;
using IdleMine.Utilities;
using System.Linq;

namespace IdleMine.Simulation
{
    public class CharacterBehaviourSystem : IExecuteSystem
    {
        private readonly Contexts contexts;

        public CharacterBehaviourSystem(Contexts contexts)
        {
            this.contexts = contexts;
        }
        public void Execute()
        {
            var characters = contexts.simulation.GetEntities(SimulationMatcher.AllOf(SimulationMatcher.Character, SimulationMatcher.CharacterRole));

            foreach (var ai in characters)
            {
                if (ai.characterRole.value == CharacterRole.Collector || ai.characterRole.value == CharacterRole.Miner)
                {
                    //Should be in mine
                    if (ai.hasAiTargetMineRoomId)
                    {
                        if (ai.hasMineRoomId == false)
                        {
                            var surfaceMineEntrancePosition = contexts.simulation.surfaceEntity.mineEntrancePosition.value;

                            var mineEntity = contexts.simulation.GetEntitiesWithMineRoomId(ai.aiTargetMineRoomId.value).SingleOrDefault(e => e.isMineRoom);

                            if (GoToAndTeleportTo(ai, surfaceMineEntrancePosition, mineEntity.mineRoomEntrance.value))
                            {
                                ai.ReplaceMineRoomId(ai.aiTargetMineRoomId.value);
                            }
                        }
                        else if (ai.mineRoomId.value != ai.aiTargetMineRoomId.value)
                        {
                            var currentMineEntrance = contexts.simulation.GetEntitiesWithMineRoomId(ai.mineRoomId.value).SingleOrDefault(e => e.isMineRoom).mineRoomEntrance.value;
                            var targetMineEntrance = contexts.simulation.GetEntitiesWithMineRoomId(ai.aiTargetMineRoomId.value).SingleOrDefault(e => e.isMineRoom).mineRoomEntrance.value;

                            if (GoToAndTeleportTo(ai, currentMineEntrance, targetMineEntrance))
                            {
                                ai.ReplaceMineRoomId(ai.aiTargetMineRoomId.value);
                            }

                        }
                        else
                        {
                            ai.ReplaceAiBehaviour(AiBehaviour.Mine);
                        }
                    }
                    else
                    //Should be on Surface
                    {
                        if (ai.hasMineRoomId)
                        {
                            var currentMineEntrance = contexts.simulation.GetEntitiesWithMineRoomId(ai.mineRoomId.value).SingleOrDefault(e => e.isMineRoom).mineRoomEntrance.value;
                            var surfaceMineEntrancePosition = contexts.simulation.surfaceEntity.mineEntrancePosition.value;

                            if (GoToAndTeleportTo(ai, currentMineEntrance, surfaceMineEntrancePosition))
                            {
                                ai.RemoveMineRoomId();
                            }
                        }
                        else
                        {
                            ai.ReplaceAiBehaviour(AiBehaviour.Surface);
                        }
                    }
                }
            }
        }

        private bool GoToAndTeleportTo(SimulationEntity ai, System.Numerics.Vector3 teleportPosition, System.Numerics.Vector3 teleportDestination)
        {
            if (AiUtilities.MoveTo(ai, teleportPosition) == false)
            {
                ai.ReplaceAiBehaviour(AiBehaviour.Moving);
            }
            else
            {
                ai.ReplacePosition(teleportDestination);
                return true;
            }

            return false;
        }
    }
}
