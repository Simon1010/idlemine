﻿namespace IdleMine.Simulation
{
    public class CharacterBehaviourFeature : Feature
    {
        public CharacterBehaviourFeature(Contexts contexts)
        {
            Add(new CharacterBehaviourSystem(contexts));
        }
    }
}
