﻿namespace IdleMine.Simulation
{
    public class MinerBehaviourFeature : Feature
    {
        public MinerBehaviourFeature(Contexts contexts)
        {
            Add(new MinerBehaviourSystem(contexts));
        }
    }
}
