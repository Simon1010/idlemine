﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Simulation
{
    [Simulation]
    public class TargetWaitingPointComponent : IComponent
    {
        public SimulationEntityId value;
    }
}
