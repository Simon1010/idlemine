﻿using Entitas;
using IdleMine.Factories;
using IdleMine.Utilities;
using System;
using System.Linq;

namespace IdleMine.Simulation
{
    public class MinerBehaviourSystem : IExecuteSystem
    {
        private readonly Contexts contexts;
        private IGroup<SimulationEntity> group;
        public MinerBehaviourSystem(Contexts contexts)
        {
            this.contexts = contexts;
            group = contexts.simulation.GetGroup(SimulationMatcher.AllOf(SimulationMatcher.CharacterRole, SimulationMatcher.AiBehaviour));
        }

        public void Execute()
        {
            var miners = group.GetEntities().Where(e => e.characterRole.value == CharacterRole.Miner);

            foreach (var miner in miners)
            {
                if (miner.aiBehaviour.value != AiBehaviour.Mine)
                {
                    SetState(miner, AiState.Idle);
                    continue;
                }


                var state = AiState.Idle;

                if (miner.hasAiState)
                {
                    state = miner.aiState.value;
                }
                else
                {
                    SetState(miner, AiState.Idle);
                }

                if (state == AiState.Idle)
                {
                    if (CanMine(miner))
                    {
                        var miningPoints = contexts.simulation.GetEntities(SimulationMatcher.MiningPoint).Where(e => e.isMiningPointOccupied == false && e.mineRoomId.value == miner.mineRoomId.value && (e.hasHealth == true && e.health.value > 0f)).ToArray();

                        var miningPoint = miningPoints[Main.Random.Next(0, miningPoints.Length)];
                        miningPoint.isMiningPointOccupied = true;
                        miner.ReplaceTargetMiningPoint(miningPoint.simulationId.value);
                        SetState(miner, AiState.MovingToMiningPoint);
                    }
                    else
                    {
                        SetState(miner, AiState.Waiting);
                    }
                }

                if (state == AiState.MovingToMiningPoint)
                {
                    if (CanMine(miner) == false)
                    {
                        SetState(miner, AiState.Idle);
                        continue;
                    }

                    if (miner.hasTargetMiningPoint)
                    {
                        var miningPoint = contexts.simulation.GetEntityWithSimulationId(miner.targetMiningPoint.value);
                        if (miningPoint == null || miningPoint.isMiningPoint == false)
                        {
                            SetState(miner, AiState.Idle);
                        }
                    }

                    if (miner.hasTargetMiningPoint)
                    {
                        var miningPoint = contexts.simulation.GetEntityWithSimulationId(miner.targetMiningPoint.value);

                        if (AiUtilities.MoveTo(miner, miningPoint.position.value))
                        {
                            SetState(miner, AiState.Mining);
                        }
                    }
                }


                if (state == AiState.Mining)
                {
                    if (CanMine(miner) == false)
                    {
                        SetState(miner, AiState.Idle);
                        continue;
                    }

                    if (miner.hasTargetMiningPoint)
                    {
                        var miningPoint = contexts.simulation.GetEntityWithSimulationId(miner.targetMiningPoint.value);

                        if (miningPoint == null)
                        {
                            SetState(miner, AiState.Idle);
                        }

                        if (miner.hasMiningPreviousTime == false)
                        {
                            miner.ReplaceMiningPreviousTime(UnityEngine.Time.time);
                        }

                        var isCoolingDown = UnityEngine.Time.time - miner.miningPreviousTime.value < miner.miningCooldown.value;

                        if (isCoolingDown == false)
                        {
                            SimulationFactory.CreateDamageEntity(contexts, 5, miningPoint.simulationId.value, miner.simulationId.value);
                            miner.ReplaceMiningPreviousTime(UnityEngine.Time.time);
                        }
                    }
                }

                if (state == AiState.Waiting)
                {
                    if (CanMine(miner))
                    {
                        SetState(miner, AiState.Idle);
                        continue;
                    }

                    if (miner.hasTargetWaitingPoint == false)
                    {
                        var waitingPoints = contexts.simulation.GetEntities(SimulationMatcher.WaitingPoint).Where(e => e.isWaitingPointOccupied == false && e.mineRoomId.value == miner.mineRoomId.value).ToArray();

                        if (waitingPoints.Any())
                        {
                            var newWaitingPoint = waitingPoints[Main.Random.Next(0, waitingPoints.Length)];
                            newWaitingPoint.isWaitingPointOccupied = true;
                            miner.ReplaceTargetWaitingPoint(newWaitingPoint.simulationId.value);
                        }
                    }

                    var waitingPoint = contexts.simulation.GetEntityWithSimulationId(miner.targetWaitingPoint.value);

                    if (AiUtilities.IsAt(miner, waitingPoint.position.value) == false)
                    {
                        SetState(miner, AiState.MovingToWaitingPoint);
                    }
                }

                if (state == AiState.MovingToWaitingPoint)
                {
                    if (CanMine(miner))
                    {
                        SetState(miner, AiState.Idle);
                        continue;
                    }

                    if (miner.hasTargetWaitingPoint)
                    {
                        var waitingPoint = contexts.simulation.GetEntityWithSimulationId(miner.targetWaitingPoint.value);
                        if (AiUtilities.IsAt(miner, waitingPoint.position.value) == false)
                        {
                            AiUtilities.MoveTo(miner, waitingPoint.position.value);
                        }
                        else
                        {
                            SetState(miner, AiState.Waiting);
                        }
                    }
                }

            }

            bool CanMine(SimulationEntity miner)
            {
                var ore = contexts.simulation.GetEntities(SimulationMatcher.Ore).SingleOrDefault(e => e.mineRoomId.value == miner.mineRoomId.value);
                if (ore != null)
                {
                    var miningPoints = contexts.simulation.GetEntities(SimulationMatcher.MiningPoint).Where(e => e.isMiningPointOccupied == false && e.mineRoomId.value == miner.mineRoomId.value && (e.hasHealth == true && e.health.value > 0f)).ToArray();

                    return (miningPoints.Any());
                }
                return false;
            }
        }

        private void SetState(SimulationEntity miner, AiState newState)
        {
            var currentState = AiState.Idle;

            if (miner.hasAiState)
            {
                currentState = miner.aiState.value;
            }

            //if (newState == AiState.Idle)
            //{
            //    if (currentState == AiState.MovingToMiningPoint || currentState == AiState.MovingToWaitingPoint)
            //    {
            //        miner.RemoveMoveTo();
            //    }
            //}

            if (newState != AiState.MovingToMiningPoint && newState != AiState.Mining)
            {
                if (currentState == AiState.MovingToMiningPoint || currentState == AiState.Mining)
                {
                    var point = contexts.simulation.GetEntityWithSimulationId(miner.targetMiningPoint.value);
                    if (point != null)
                    {
                        point.isMiningPointOccupied = false;
                    }
                    miner.RemoveTargetMiningPoint();
                }
            }

            if (newState != AiState.MovingToWaitingPoint && newState != AiState.Waiting)
            {
                if (currentState == AiState.MovingToWaitingPoint || currentState == AiState.Waiting)
                {
                    var point = contexts.simulation.GetEntityWithSimulationId(miner.targetWaitingPoint.value);
                    if (point != null)
                    {
                        point.isWaitingPointOccupied = false;
                    }
                    miner.RemoveTargetWaitingPoint();
                }
            }

            miner.ReplaceAiState(newState);
        }
    }
}
