﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Simulation
{
    [Simulation]
    public class TargetMiningPointComponent : IComponent
    {
        public SimulationEntityId value;
    }
}
