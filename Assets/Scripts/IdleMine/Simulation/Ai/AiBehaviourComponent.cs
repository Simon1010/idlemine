﻿using Entitas;

namespace IdleMine.Simulation
{
    [Simulation]
    public class AiBehaviourComponent : IComponent
    {
        public AiBehaviour value;
    }

    public enum AiBehaviour
    {
        Surface,
        Mine,
        Moving
    }
}
