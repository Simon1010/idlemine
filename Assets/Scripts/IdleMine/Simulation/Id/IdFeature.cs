﻿namespace IdleMine.Simulation
{
    public class IdFeature : Feature
    {
        public IdFeature(Contexts contexts)
        {
            Add(new AssignIdSystem(contexts));
        }
    }
}
