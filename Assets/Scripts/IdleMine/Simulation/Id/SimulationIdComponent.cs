﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using IdleMine.Utilities;

namespace IdleMine.Simulation
{
    [Simulation]
    public class SimulationIdComponent : IComponent
    {
        [PrimaryEntityIndex] public SimulationEntityId value;
    }
}
