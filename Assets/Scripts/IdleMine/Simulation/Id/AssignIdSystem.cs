﻿using Entitas;

namespace IdleMine.Simulation
{
    public class AssignIdSystem : IInitializeSystem
    {
        private readonly Contexts contexts;

        public AssignIdSystem(Contexts contexts)
        {
            this.contexts = contexts;
        }

        public void Initialize()
        {
            contexts.simulation.OnEntityCreated += AddId;
        }

        private void AddId(IContext context, IEntity entity)
        {
            var simulationEntity = entity as SimulationEntity;
            simulationEntity.ReplaceSimulationId(new Utilities.SimulationEntityId(entity.creationIndex));
        }
    }
}
