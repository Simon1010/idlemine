﻿
namespace IdleMine.Simulation
{
    public class SimulationFeature : Feature
    {
        public SimulationFeature(Contexts contexts)
        {
            Add(new IdFeature(contexts));
            Add(new MineFeature(contexts));
            Add(new SurfaceFeature(contexts));
            Add(new AiFeature(contexts));
            Add(new MoveFeature(contexts));
            Add(new HealthFeature(contexts));
        }
    }
}
