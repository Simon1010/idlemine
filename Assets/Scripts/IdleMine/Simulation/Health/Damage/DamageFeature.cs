﻿namespace IdleMine.Simulation
{
    public class DamageFeature : Feature
    {
        public DamageFeature(Contexts contexts)
        {
            Add(new DealDamageToHealthSystem(contexts));
        }
    }
}
