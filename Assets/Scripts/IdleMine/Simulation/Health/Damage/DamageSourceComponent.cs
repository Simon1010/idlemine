﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Simulation
{
    [Simulation]
    public class DamageSourceComponent : IComponent
    {
        public SimulationEntityId value;
    }
}
