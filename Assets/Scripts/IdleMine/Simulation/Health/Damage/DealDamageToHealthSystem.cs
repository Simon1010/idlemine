﻿using System.Collections.Generic;
using Entitas;

namespace IdleMine.Simulation
{
    public class DealDamageToHealthSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public DealDamageToHealthSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var target = contexts.simulation.GetEntityWithSimulationId(entity.damageTarget.value);

                var health = target.health.value;
                health -= entity.dealDamage.value;
                target.ReplaceHealth(health);
                entity.isDestroyed = true;
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.hasDealDamage && entity.hasDamageTarget;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.DealDamage);
        }
    }
}
