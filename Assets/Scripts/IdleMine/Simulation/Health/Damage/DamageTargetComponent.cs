﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Simulation
{
    [Simulation]
    public class DamageTargetComponent : IComponent
    {
        public SimulationEntityId value;
    }
}
