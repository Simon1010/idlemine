﻿namespace IdleMine.Simulation
{
    public class HealthFeature : Feature
    {
        public HealthFeature(Contexts contexts)
        {
            Add(new DamageFeature(contexts));
        }
    }
}
