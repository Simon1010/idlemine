﻿using System.Collections.Generic;
using Entitas;

namespace IdleMine.Simulation
{
    public class DestroyOreWhenNoOrePiecesLeftSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public DestroyOreWhenNoOrePiecesLeftSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var ore in entities)
            {
                //TODO: Create test for this
                if(ore.orePiecesLeft.value == 0)
                {
                    ore.isDestroyed = true;
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.hasOrePiecesLeft;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.AllOf(SimulationMatcher.Ore, SimulationMatcher.OrePiecesLeft));
        }
    }
}
