﻿using System.Collections.Generic;
using Entitas;

namespace IdleMine.Simulation
{
    public class GetMiningPointHealthFromOreWhenHealthIsZeroSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public GetMiningPointHealthFromOreWhenHealthIsZeroSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var miningPoint in entities)
            {
                var isInNeedOfHealth = true;

                if (miningPoint.hasHealth)
                {
                    isInNeedOfHealth = miningPoint.health.value <=- 0;
                }

                if (isInNeedOfHealth)
                {
                    var ore = contexts.simulation.GetEntityWithSimulationId(miningPoint.targetOre.value);

                    if (ore.orePiecesLeft.value > 0)
                    {
                        miningPoint.ReplaceHealth(ore.oreToughness.value);
                    }
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isMiningPoint;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.AnyOf(SimulationMatcher.MiningPoint, SimulationMatcher.Health));
        }
    }
}
