﻿namespace IdleMine.Simulation
{
    public class MiningPointHealthFeature : Feature
    {
        public MiningPointHealthFeature(Contexts contexts)
        {
            Add(new GetMiningPointHealthFromOreWhenHealthIsZeroSystem(contexts));
        }

    }
}
