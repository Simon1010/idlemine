﻿namespace IdleMine.Simulation
{
    public class OreFeature : Feature
    {
        public OreFeature(Contexts contexts)
        {
            Add(new DropOreWhenMiningPointHealthIsZeroSystem(contexts));
            Add(new GetMiningPointHealthFromOreWhenHealthIsZeroSystem(contexts));
            Add(new DestroyOreWhenNoOrePiecesLeftSystem(contexts));
            Add(new DestroyMiningPointWhenOreDestroyedAndMiningPointHealthIsZero(contexts));
        }
    }
}
