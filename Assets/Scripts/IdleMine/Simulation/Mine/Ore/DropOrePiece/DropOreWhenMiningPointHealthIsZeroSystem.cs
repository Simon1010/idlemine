﻿using System.Collections.Generic;
using Entitas;

namespace IdleMine.Simulation
{
    public class DropOreWhenMiningPointHealthIsZeroSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public DropOreWhenMiningPointHealthIsZeroSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var miningPoint in entities)
            {
                if (miningPoint.health.value <= 0)
                {
                    var ore = contexts.simulation.GetEntityWithSimulationId(miningPoint.targetOre.value);
                    ore.ReplaceOrePiecesLeft(ore.orePiecesLeft.value - 1);
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isMiningPoint && entity.hasHealth;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.AllOf(SimulationMatcher.Health, SimulationMatcher.MiningPoint));
        }
    }
}
