﻿using System.Collections.Generic;
using Entitas;

namespace IdleMine.Simulation
{
    public class DestroyMiningPointWhenOreDestroyedAndMiningPointHealthIsZero : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public DestroyMiningPointWhenOreDestroyedAndMiningPointHealthIsZero(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            //TODO: Create test for this
            foreach (var miningPoint in entities)
            {
                if (miningPoint.health.value <= 0)
                {
                    var ore = contexts.simulation.GetEntityWithSimulationId(miningPoint.targetOre.value);
                    if (ore.isDestroyed)
                    {
                        miningPoint.isDestroyed = true;
                    }
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.hasHealth;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.AllOf(SimulationMatcher.MiningPoint, SimulationMatcher.Health));
        }
    }
}
