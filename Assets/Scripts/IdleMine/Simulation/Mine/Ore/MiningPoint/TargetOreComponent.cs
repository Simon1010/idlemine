﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Simulation
{
    [Simulation]
    public class TargetOreComponent : IComponent
    {
        public SimulationEntityId value;
    }
}
