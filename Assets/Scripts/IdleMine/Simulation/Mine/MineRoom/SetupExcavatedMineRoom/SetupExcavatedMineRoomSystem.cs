﻿using System.Collections.Generic;
using Entitas;
using IdleMine.Factories;

namespace IdleMine.Simulation
{
    public class SetupExcavatedMineRoomSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public SetupExcavatedMineRoomSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                //Create Mine Entrance
                entity.AddMineRoomEntrance(Main.TempMineData.MineEntrance);

                //Create Ore
                var ore = contexts.simulation.CreateEntity();
                ore.isOre = true;
                ore.AddPosition(Main.TempMineData.OrePosition);
                ore.AddMineRoomId(entity.mineRoomId.value);
                ore.AddOreToughness(3);
                ore.AddOrePiecesLeft(5);

                //Create Cart
                var cart = SimulationFactory.CreateCart(contexts);
                cart.AddMineRoomId(entity.mineRoomId.value);
                cart.AddCartStartPosition(Main.TempMineData.CartStart);
                cart.AddCartEndPosition(Main.TempMineData.CartEnd);
                cart.AddPosition(Main.TempMineData.CartStart);

                //Create Waiting Points
                var waitingPoints = Main.TempMineData.WaitingPositions;
                foreach (var point in waitingPoints)
                {
                    var pointEntity = contexts.simulation.CreateEntity();
                    pointEntity.isWaitingPoint = true;
                    pointEntity.AddPosition(point);
                    pointEntity.AddMineRoomId(entity.mineRoomId.value);
                }

                //Create Mining Points
                var miningPoints = Main.TempMineData.MiningPositions;
                foreach (var point in miningPoints)
                {
                    var pointEntity = contexts.simulation.CreateEntity();
                    pointEntity.isMiningPoint = true;
                    pointEntity.AddTargetOre(ore.simulationId.value);
                    pointEntity.AddPosition(point);
                    pointEntity.AddMineRoomId(entity.mineRoomId.value);
                } //Create Storing Points
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isMineRoomExcavated;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.MineRoomExcavated);
        }
    }
}
