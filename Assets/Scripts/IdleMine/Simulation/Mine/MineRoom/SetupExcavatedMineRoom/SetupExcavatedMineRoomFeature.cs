﻿namespace IdleMine.Simulation
{
    public class SetupExcavatedMineRoomFeature : Feature
    {
        public SetupExcavatedMineRoomFeature(Contexts contexts)
        {
            Add(new SetupExcavatedMineRoomSystem(contexts));
        }
    }
}
