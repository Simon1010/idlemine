﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using IdleMine.Factories;
using IdleMine.Utilities;

namespace IdleMine.Simulation
{
    public class CreateMineRoomSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public CreateMineRoomSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            var lastRow = MineUtilities.MineMinimumRows;

            var lastExcavatedRoom = contexts.simulation.GetEntities(SimulationMatcher.AllOf(SimulationMatcher.MineRoomExcavated, SimulationMatcher.MineRoom)).OrderBy(e => e.mineRoomId.value).LastOrDefault();

            if (lastExcavatedRoom != null)
            {
                var roomRow = MineUtilities.GetMineRow(lastExcavatedRoom.mineRoomId.value);
                if (lastRow < roomRow)
                {
                    lastRow = roomRow;
                }
            }

            lastRow += MineUtilities.MineBottomPadding;

            for (int row = 0; row < lastRow; row++)
            {
                for (int column = 0; column < MineUtilities.MineColumns; column++)
                {
                    var mineRoomId = MineUtilities.GetMineRoomId(column, row);
                    var mineRoom = contexts.simulation.GetEntitiesWithMineRoomId(mineRoomId).Where(e => e.isMineRoom).SingleOrDefault();
                    if (mineRoom == null)
                    {
                        mineRoom = SimulationFactory.CreateMineRoom(contexts, mineRoomId);
                    }
                }
            }


        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isStartGame || entity.isMineRoomExcavated;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.AnyOf(SimulationMatcher.StartGame, SimulationMatcher.MineRoomExcavated));
        }
    }
}
