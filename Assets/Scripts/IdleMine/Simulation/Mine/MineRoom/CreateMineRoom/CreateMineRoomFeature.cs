﻿namespace IdleMine.Simulation
{
    public class CreateMineRoomFeature : Feature
    {
        public CreateMineRoomFeature(Contexts contexts)
        {
            Add(new CreateMineRoomSystem(contexts));
        }
    }
}
