﻿
namespace IdleMine.Simulation
{
    public class PositionMineRoomFeature : Feature
    {
        public PositionMineRoomFeature(Contexts contexts)
        {
            Add(new PositionMineRoomSystem(contexts));
        }
    }
}
