﻿using System.Collections.Generic;
using System.Numerics;
using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Simulation
{
    public class PositionMineRoomSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public PositionMineRoomSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var mineId = entity.mineRoomId.value;
                var minePosition = MineUtilities.GetMineRoomPosition(mineId);
                entity.ReplacePosition(minePosition);
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.hasMineRoomId;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.AllOf(SimulationMatcher.MineRoom, SimulationMatcher.MineRoomId));
        }
    }
}
