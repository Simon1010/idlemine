﻿namespace IdleMine.Simulation
{
    public class MineRoomFeature : Feature
    {
        public MineRoomFeature(Contexts contexts)
        {
            Add(new CreateMineRoomFeature(contexts));
            Add(new PositionMineRoomFeature(contexts));
            Add(new SetupExcavatedMineRoomFeature(contexts));
        }
    }
}
