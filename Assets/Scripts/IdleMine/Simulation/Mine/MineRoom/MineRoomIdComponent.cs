using Entitas;

namespace IdleMine.Simulation
{
    [Simulation]
    public class MineRoomIdComponent : IComponent
    {
        [Entitas.CodeGeneration.Attributes.EntityIndex] public int value;
    }
}