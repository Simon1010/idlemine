﻿namespace IdleMine.Simulation
{
    public class MineFeature : Feature
    {
        public MineFeature(Contexts contexts)
        {
            Add(new MineRoomFeature(contexts));
            Add(new OreFeature(contexts));
        }
    }
}
