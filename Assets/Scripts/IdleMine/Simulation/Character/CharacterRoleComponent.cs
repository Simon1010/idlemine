﻿using Entitas;

namespace IdleMine.Simulation
{
    [Simulation]
    public class CharacterRoleComponent : IComponent
    {
        public CharacterRole value;
    }

    public enum CharacterRole
    {
        Miner,
        Storer,
        Collector
    }
}
