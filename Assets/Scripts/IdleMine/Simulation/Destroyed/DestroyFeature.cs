﻿
namespace IdleMine.Simulation
{
    public class DestroyFeature : Feature
    {
        public DestroyFeature(Contexts contexts)
        {
            Add(new DependencyFeature(contexts));
            Add(new DestroyEntitiesSystem(contexts));
        }
    }
}
