﻿// IDestroyed: "I'm an Entity, I can have a DestroyedComponent"
using Entitas;
using System.Collections.Generic;

public interface IDestroyableEntity : IEntity, IDestroyedEntity { }
public partial class SimulationEntity : IDestroyableEntity { }
public partial class ActionEntity : IDestroyableEntity { }
public partial class CommandEntity : IDestroyableEntity { }


namespace IdleMine.Simulation
{

    // inherit from MultiReactiveSystem using the IDestroyed interface defined above
    public class DestroyEntitiesSystem : MultiReactiveSystem<IDestroyableEntity, Contexts>
    {
        // base class takes in all contexts, not just one as in normal ReactiveSystems
        public DestroyEntitiesSystem(Contexts contexts) : base(contexts)
        {
        }

        // return an ICollector[] with a collector from each context
        protected override ICollector[] GetTrigger(Contexts contexts)
        {
            return new ICollector[] {
            contexts.simulation.CreateCollector(SimulationMatcher.Destroyed),
            contexts.action.CreateCollector(ActionMatcher.Destroyed),
            contexts.command.CreateCollector(CommandMatcher.Destroyed)
        };
        }

        protected override bool Filter(IDestroyableEntity entity)
        {
            return entity.isDestroyed;
        }

        protected override void Execute(List<IDestroyableEntity> entities)
        {
            foreach (var e in entities)
            {
                e.Destroy();
            }
        }
    }
}
