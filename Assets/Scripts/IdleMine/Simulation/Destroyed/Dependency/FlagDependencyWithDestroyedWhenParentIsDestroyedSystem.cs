﻿using System.Collections.Generic;
using System.Linq;
using Entitas;

namespace IdleMine.Simulation
{
    public class FlagDependencyWithDestroyedWhenParentIsDestroyedSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public FlagDependencyWithDestroyedWhenParentIsDestroyedSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }


        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                DestroyDependencies(entity);
                entity.isDestroyed = true;
            }
        }

        private void DestroyDependencies(SimulationEntity entity)
        {
            var dependencies = contexts.simulation.GetEntitiesWithDependency(entity.simulationId.value).Where(e => e.isDestroyed == false && e.simulationId.value != entity.simulationId.value);

            foreach (var dependency in dependencies)
            {
                DestroyDependencies(dependency);
                dependency.isDestroyed = true;
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isDestroyed;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.Destroyed);
        }
    }
}
