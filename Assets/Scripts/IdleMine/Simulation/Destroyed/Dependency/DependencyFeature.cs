﻿namespace IdleMine.Simulation
{
    public class DependencyFeature : Feature
    {
        public DependencyFeature(Contexts contexts)
        {
            Add(new FlagDependencyWithDestroyedWhenParentIsDestroyedSystem(contexts));
        }
    }
}
