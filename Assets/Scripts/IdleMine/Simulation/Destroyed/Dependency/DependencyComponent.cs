﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using IdleMine.Utilities;

namespace IdleMine.Simulation
{
    [Simulation]
    public class DependencyComponent : IComponent
    {
        [EntityIndex] public SimulationEntityId value;
    }
}
