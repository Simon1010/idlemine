﻿namespace IdleMine.Simulation
{
    public class SurfaceFeature : Feature
    {
        public SurfaceFeature(Contexts contexts)
        {
            Add(new CreateSurfaceFeature(contexts));
        }
    }
}
