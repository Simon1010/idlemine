﻿namespace IdleMine.Simulation
{
    public class CreateSurfaceFeature : Feature
    {
        public CreateSurfaceFeature(Contexts contexts)
        {
            Add(new CreateSurfaceSystem(contexts));
        }
    }
}
