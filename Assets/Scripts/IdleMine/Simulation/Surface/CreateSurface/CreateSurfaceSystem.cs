﻿using System.Collections.Generic;
using Entitas;
using IdleMine.Factories;

namespace IdleMine.Simulation
{
    internal class CreateSurfaceSystem : ReactiveSystem<SimulationEntity>
    {
        private Contexts contexts;

        public CreateSurfaceSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var surface = SimulationFactory.CreateSurface(contexts);
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isStartGame;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.StartGame);
        }
    }
}