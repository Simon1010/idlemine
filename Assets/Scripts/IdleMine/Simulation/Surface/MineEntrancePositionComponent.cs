﻿using Entitas;
using System.Numerics;

namespace IdleMine.Simulation
{
    [Simulation]
    public class MineEntrancePositionComponent : IComponent
    {
        public Vector3 value;
    }
}
