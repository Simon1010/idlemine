﻿using Entitas;
using System.Numerics;

namespace IdleMine.Simulation
{
    [Simulation]
    public class CartEndPositionComponent : IComponent
    {
        public Vector3 value;
    }
}
