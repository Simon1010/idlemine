﻿using Entitas;
using System.Numerics;

namespace IdleMine.Simulation
{
    [Simulation]
    public class CartStartPositionComponent : IComponent
    {
        public Vector3 value;
    }
}
