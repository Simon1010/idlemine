﻿namespace IdleMine.Simulation
{
    public class MoveFeature : Feature
    {
        public MoveFeature(Contexts contexts)
        {
            Add(new MoveToFeature(contexts));
            Add(new VelocityFeature(contexts));
        }
    }
}
