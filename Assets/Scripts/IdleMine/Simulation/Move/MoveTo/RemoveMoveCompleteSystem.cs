﻿using Entitas;

namespace IdleMine.Simulation
{
    public class RemoveMoveCompleteSystem : IExecuteSystem
    {
        private readonly Contexts contexts;
        private IGroup<SimulationEntity> group;
        public RemoveMoveCompleteSystem(Contexts contexts)
        {
            this.contexts = contexts;
            group = contexts.simulation.GetGroup(SimulationMatcher.MoveComplete);
        }

        public void Execute()
        {
            foreach (var entity in group.GetEntities())
            {
                entity.isMoveComplete = false;
            }
        }
    }
}
