﻿using Entitas;
using System;
using System.Numerics;

namespace IdleMine.Simulation
{
    public class MoveToSystem : IExecuteSystem
    {
        private readonly Contexts contexts;
        private IGroup<SimulationEntity> group;
        public MoveToSystem(Contexts contexts)
        {
            this.contexts = contexts;
            group = contexts.simulation.GetGroup(SimulationMatcher.MoveTo);
        }

        public void Execute()
        {
            foreach (var entity in group.GetEntities())
            {
                var position = entity.position.value;
                var velocity = entity.hasVelocity ? entity.velocity.value : Vector3.Zero;
                var moveSpeed = entity.hasMoveTo ? entity.moveSpeed.value : 0f;
                Vector3 destination = entity.moveTo.value;

                var distance = Vector3.Distance(position, destination);

                if (distance == 0f)
                {
                    entity.RemoveVelocity();
                    entity.RemoveMoveTo();
                    entity.isMoveComplete = true;
                }
                else
                {
                    var xVelocity = GetVelocity(velocity.X, GetVelocity(position.X, destination.X, moveSpeed), moveSpeed);
                    var yVelocity = GetVelocity(velocity.Y, GetVelocity(position.Y, destination.Y, moveSpeed), moveSpeed);
                    var zVelocity = 0f;
                    velocity += new Vector3(xVelocity, yVelocity, zVelocity);
                    entity.ReplaceVelocity(velocity);
                }
            }

        }

        private float GetVelocity(float position, float destination, float moveSpeed)
        {
            var difference = destination - position;

            if (Math.Abs(difference) < moveSpeed)
            {
                return difference;
            }
            else
            {
                return moveSpeed * Math.Sign(difference);
            }
        }
    }
}
