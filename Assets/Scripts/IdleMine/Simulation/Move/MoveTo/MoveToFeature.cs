﻿namespace IdleMine.Simulation
{
    public class MoveToFeature : Feature
    {
        public MoveToFeature(Contexts contexts)
        {
            Add(new RemoveMoveCompleteSystem(contexts));
            Add(new MoveToSystem(contexts));
        }
    }
}
