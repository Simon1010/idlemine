﻿using Entitas;

namespace IdleMine.Simulation
{
    public class ApplyVelocitySystem : IExecuteSystem
    {
        private readonly Contexts contexts;
        private IGroup<SimulationEntity> group;
        public ApplyVelocitySystem(Contexts contexts)
        {
            this.contexts = contexts;
            group = contexts.simulation.GetGroup(SimulationMatcher.Velocity);
        }

        public void Execute()
        {
            foreach (var entity in group.GetEntities())
            {
                var velocity = entity.velocity.value;
                var position = entity.position.value;
                position += velocity;
                entity.ReplacePosition(position);
            }
        }
    }
}
