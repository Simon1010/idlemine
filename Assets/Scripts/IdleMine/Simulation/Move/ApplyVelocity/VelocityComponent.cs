﻿using Entitas;
using System.Numerics;

namespace IdleMine.Simulation
{
    [Simulation]
    public class VelocityComponent : IComponent
    {
        public Vector3 value;
    }
}
