﻿namespace IdleMine.Simulation
{
    public class VelocityFeature : Feature
    {
        public VelocityFeature(Contexts contexts)
        {
            Add(new ApplyVelocitySystem(contexts));
            Add(new ApplyFrictionSystem(contexts));
            Add(new RemoveVelocityWhenZeroSystem(contexts));
        }
    }
}
