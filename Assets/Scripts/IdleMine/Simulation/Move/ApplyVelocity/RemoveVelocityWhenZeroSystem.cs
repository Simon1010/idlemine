﻿using Entitas;
using System;

namespace IdleMine.Simulation
{
    public class RemoveVelocityWhenZeroSystem : IExecuteSystem
    {
        private readonly Contexts contexts;
        private IGroup<SimulationEntity> group;
        public RemoveVelocityWhenZeroSystem(Contexts contexts)
        {
            this.contexts = contexts;
            group = contexts.simulation.GetGroup(SimulationMatcher.Velocity);
        }

        public void Execute()
        {
            foreach (var entity in group.GetEntities())
            {
                var velocity = entity.velocity.value;

                var totalVelocity = Math.Abs(velocity.X + velocity.Y + velocity.Z);
                if (totalVelocity <= 0.0001f)
                {
                    entity.RemoveVelocity();
                }
            }
        }
    }
}
