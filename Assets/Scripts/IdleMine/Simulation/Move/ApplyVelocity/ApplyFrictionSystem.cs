﻿using Entitas;

namespace IdleMine.Simulation
{
    public class ApplyFrictionSystem : IExecuteSystem
    {
        private readonly Contexts contexts;
        private IGroup<SimulationEntity> group;
        public ApplyFrictionSystem(Contexts contexts)
        {
            this.contexts = contexts;
            group = contexts.simulation.GetGroup(SimulationMatcher.Velocity);
        }

        public void Execute()
        {
            foreach (var entity in group.GetEntities())
            {
                var velocity = entity.velocity.value;
                velocity *= 0.9f;
                entity.ReplaceVelocity(velocity);
            }
        }
    }
}
