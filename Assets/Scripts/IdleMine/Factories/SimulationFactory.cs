﻿using IdleMine.Utilities;
using System;
using System.Numerics;

namespace IdleMine.Factories
{
    public static class SimulationFactory
    {
        public static SimulationEntity CreateMineRoom(Contexts contexts, int mineRoomId)
        {
            var entity = contexts.simulation.CreateEntity();
            entity.isMineRoom = true;
            entity.AddMineRoomId(mineRoomId);
            entity.AddPosition(Vector3.Zero);
            entity.isMineRoomExcavated = true;
            return entity;
        }

        public static SimulationEntity CreateCart(Contexts contexts)
        {
            var entity = contexts.simulation.CreateEntity();
            entity.isCart = true;
            return entity;
        }

        internal static object CreateSurface(Contexts contexts)
        {
            var entity = contexts.simulation.CreateEntity();
            entity.isSurface = true;
            entity.AddPosition(Vector3.Zero);
            entity.AddMineEntrancePosition(new Vector3(-26.362f, -2.545f, 0f));
            return entity;
        }

        public static void CreateDamageEntity(Contexts contexts, float damage, SimulationEntityId target, SimulationEntityId source)
        {
            var damageEntity = contexts.simulation.CreateEntity();
            damageEntity.AddDealDamage(damage);
            damageEntity.AddDamageTarget(target);
            damageEntity.AddDamageSource(source);
        }
    }
}
