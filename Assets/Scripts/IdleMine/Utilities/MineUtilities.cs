﻿using System;
using System.Numerics;

namespace IdleMine.Utilities
{
    public static class MineUtilities
    {
        public const int MineTopPadding = 0;
        public const int MineBottomPadding = 1;

        public const int MineMinimumRows = 3;

        public const int MineColumns = 3;
        private const int MinePositionColumnOffset = -1;
        private const int MinePositionRowOffset = -2;

        public static Vector3 GetMineRoomPosition(int mineId)
        {
            return GetMineGridPosition(mineId).ToVector3() * GetMineSize().ToVector3();
        }

        public static Vector2 GetMineGridPosition(int mineId)
        {
            return new Vector2(GetMineColumn(mineId) + MinePositionColumnOffset, -GetMineRow(mineId) + MinePositionRowOffset);
        }

        public static int GetMineColumn(int mineId)
        {
            return mineId % MineColumns;
        }

        internal static object GetMineRoomIdFromPosition(Vector3 position)
        {
            var closestColumnId = (int)Math.Round(position.X / GetMineSize().X) + MinePositionColumnOffset;
            var closestRowId = (int)Math.Round(position.Y / GetMineSize().Y) + MinePositionRowOffset;
            var mineId = GetMineRoomId(closestColumnId, closestRowId);
            return mineId;
        }

        public static int GetMineRow(int mineId)
        {
            return mineId / MineColumns;
        }


        internal static int GetMineRoomId(int column, int row)
        {
            var mineId = column;
            mineId += row * MineColumns;
            return mineId;
        }


        public static Vector2 GetMineSize()
        {
            return new Vector2(18f, 6.66f);
        }

    }
}
