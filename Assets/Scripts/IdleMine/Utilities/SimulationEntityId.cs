﻿using System;

namespace IdleMine.Utilities
{
    public struct SimulationEntityId : IEquatable<SimulationEntityId>
    {

        private int value;

        public SimulationEntityId(int value)
        {
            this.value = value;
        }

        public static bool operator !=(SimulationEntityId a, SimulationEntityId b)
        {
            return a.value != b.value;
        }

        public static bool operator ==(SimulationEntityId a, SimulationEntityId b)
        {
            return a.value == b.value;
        }

        public bool Equals(SimulationEntityId other)
        {
            return other.value == value;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType() || obj.GetHashCode() != GetHashCode())
            {
                return false;
            }

            var other = (SimulationEntityId)obj;
            return other.value == value;
        }

        public override int GetHashCode()
        {
            return value;
        }

        public static SimulationEntityId Empty
        {
            get
            {
                return new SimulationEntityId();
            }
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }
}
