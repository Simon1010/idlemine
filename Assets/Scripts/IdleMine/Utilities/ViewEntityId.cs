﻿using System;

namespace IdleMine.Utilities
{
    public struct ViewEntityId : IEquatable<ViewEntityId>
    {

        private int value;

        public ViewEntityId(int value)
        {
            this.value = value;
        }

        public static bool operator !=(ViewEntityId a, ViewEntityId b)
        {
            return a.value != b.value;
        }

        public static bool operator ==(ViewEntityId a, ViewEntityId b)
        {
            return a.value == b.value;
        }

        public bool Equals(ViewEntityId other)
        {
            return other.value == value;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType() || obj.GetHashCode() != GetHashCode())
            {
                return false;
            }

            var other = (ViewEntityId)obj;
            return other.value == value;
        }

        public override int GetHashCode()
        {
            return value;
        }

        public static ViewEntityId Empty
        {
            get
            {
                return new ViewEntityId();
            }
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }
}
