﻿using System;
using System.Numerics;

namespace IdleMine.Utilities
{
    public static class AiUtilities
    {
        public static bool MoveTo(SimulationEntity entity, Vector3 position, float distanceThreshold = 0f)
        {
            var isAtPosition = IsAt(entity, position, distanceThreshold);
            if (isAtPosition == false)
            {
                if (entity.hasMoveTo == false || entity.moveTo.value != position)
                {
                    entity.ReplaceMoveTo(position);
                }
            }
            else
            {
                if (entity.hasMoveTo)
                    entity.RemoveMoveTo();
            }

            return isAtPosition;
        }

        internal static bool IsAt(SimulationEntity entity, Vector3 position, float distanceThreshold = 0f)
        {
            var distance = Vector3.Distance(entity.position.value, position);
            return distance <= distanceThreshold;
        }
    }
}
