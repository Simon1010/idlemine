﻿
using UnityEngine;

namespace IdleMine.Unity
{
    public class HideOnStart : MonoBehaviour
    {
        private void Start()
        {
            gameObject.SetActive(false);
        }
    }
}
