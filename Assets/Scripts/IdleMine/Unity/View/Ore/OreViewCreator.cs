﻿using IdleMine.Listener;
using IdleMine.Utilities;
using UnityEngine;

namespace IdleMine.Unity
{
    public class OreViewCreator : UnityBaseViewCreator, IListenerAnyOreCreated
    {
        public OreView OrePrefab;

        protected override void OnEnabled()
        {
            listenerEntity.AddListenerAnyOreCreated(this);
        }

        protected override void OnDisabled()
        {
        }

        void IListenerAnyOreCreated.OnAnyOreCreated(SimulationEntityId simulationEntityId)
        {
            var view = CreateGameObject<OreView>(OrePrefab, simulationEntityId);
        }

    }
}
