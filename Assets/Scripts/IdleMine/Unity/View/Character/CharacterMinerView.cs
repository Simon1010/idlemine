﻿using IdleMine.Listener;
using UnityEngine;

namespace IdleMine.Unity
{
    public class CharacterMinerView : UnityBaseView, IAnimationNameListener
    {
        public SpriteRenderer HeadRenderer;
        public SpriteRenderer WeaponRenderer;

        public Animator Animator;

        public int MiningAttackFrame;

        protected override void OnInitialize()
        {
            CreateView();
            viewEntity.AddAnimationNameListener(this);
        }

        void IAnimationNameListener.OnAnimationName(ViewEntity entity, string value)
        {
            Animator.Play(value);
        }

        protected override void OnTeardown()
        {
        }


    }
}
