﻿using IdleMine.Listener;
using IdleMine.Unity;
using IdleMine.Utilities;

namespace IdleMine.Unity
{
    public class CharacterViewCreator : UnityBaseViewCreator, IListenerAnyCharacter
    {
        public CharacterMinerView MinerView;

        protected override void OnDisabled()
        {
        }

        protected override void OnEnabled()
        {
            listenerEntity.AddListenerAnyCharacter(this);
        }

        void IListenerAnyCharacter.OnAnyCharacterCreated(SimulationEntityId simulationEntityId)
        {
            var characterView = CreateGameObject<CharacterMinerView>(MinerView, simulationEntityId);
        }
    }
}
