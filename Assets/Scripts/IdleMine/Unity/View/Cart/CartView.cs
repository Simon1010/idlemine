﻿using IdleMine.Listener;
using IdleMine.Utilities;
using System.Linq;
using UnityEngine;

namespace IdleMine.Unity
{
    public class CartView : UnityBaseView
    {
        public SpriteRenderer OrePileRenderer;
        public Transform OrePileTransform;
        public Vector2 OrePileHiddenPosition;

        protected override void OnInitialize()
        {
            CreateView();
        }

        protected override void OnTeardown()
        {
        }

    }
}
