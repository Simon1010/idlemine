﻿using IdleMine.Listener;
using IdleMine.Utilities;
using System.Collections.Generic;
using UnityEngine;

namespace IdleMine.Unity
{
    public class CartViewCreator : UnityBaseViewCreator, IListenerAnyCart
    {
        public CartView ViewPrefab;
        private Dictionary<SimulationEntityId, CartView> views;

        protected override void OnDisabled()
        {
        }

        protected override void OnEnabled()
        {
            listenerEntity.AddListenerAnyCart(this);
            views = new Dictionary<SimulationEntityId, CartView>();
        }

        void IListenerAnyCart.OnCartCreated(SimulationEntityId id)
        {
            if (views.ContainsKey(id) == false)
            {
                var view = CreateGameObject<CartView>(ViewPrefab, id);
                views.Add(id, view);
            }
        }
    }
}
