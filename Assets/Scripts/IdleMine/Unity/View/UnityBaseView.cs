﻿using IdleMine.Listener;
using IdleMine.Utilities;
using UnityEngine;
using Entitas.Unity;

namespace IdleMine.Unity
{
    public abstract class UnityBaseView : MonoBehaviour, IListenerPosition, IParentListener, IListenerDestroyed
    {
        protected SimulationEntityId _id = SimulationEntityId.Empty;
        protected ViewEntity viewEntity;
        protected ListenerEntity listenerEntity;

        public void Initialize(SimulationEntityId id)
        {
            _id = id;

            listenerEntity = Contexts.sharedInstance.listener.CreateEntity();
            listenerEntity.AddListenerPosition(this);
            listenerEntity.AddListenerDestroyed(this);
            listenerEntity.AddListenerTarget(_id);
            listenerEntity.isTriggerOnAdd = true;

            var simEntity = Contexts.sharedInstance.simulation.GetEntityWithSimulationId(_id);
            gameObject.Link(simEntity);
            OnInitialize();
        }


        protected abstract void OnInitialize();
        protected abstract void OnTeardown();


        public void Teardown()
        {
            gameObject.Unlink();
            listenerEntity.Destroy();
            if (viewEntity != null)
            {
                viewEntity.Destroy();
            }
            Destroy(gameObject);
            OnTeardown();
        }

        protected void CreateView()
        {
            viewEntity = Contexts.sharedInstance.view.CreateEntity();
            viewEntity.AddParentListener(this);
            viewEntity.AddTransform(transform);
            viewEntity.AddViewTarget(_id);
            //gameObject.Link(viewEntity);
        }

        void IListenerPosition.OnPosition(System.Numerics.Vector3 value)
        {
            transform.localPosition = value.ToUnityVector3();
        }
        void IParentListener.OnParent(ViewEntity entity, Transform value)
        {
            transform.SetParent(value, false);
        }

        void IListenerDestroyed.OnDestroyed()
        {
            Teardown();
        }

        public void OnAnimationComplete()
        {
            viewEntity.isAnimationComplete = true;
        }
    }
}
