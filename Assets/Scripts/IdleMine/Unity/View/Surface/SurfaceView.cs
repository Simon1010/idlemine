﻿using UnityEngine;

namespace IdleMine.Unity
{
    public class SurfaceView : UnityBaseView
    {
        public Transform EntityContainer;
        public Transform EntityUiContainer;

        protected override void OnInitialize()
        {
            CreateView();
        }

        protected override void OnTeardown()
        {
        }

    }
}
