﻿using IdleMine.Listener;
using IdleMine.Utilities;
using UnityEngine;

namespace IdleMine.Unity
{
    public class SurfaceViewCreator : UnityBaseViewCreator, IListenerSurfaceCreated
    {
        public SurfaceView SurfacePrefab;

        protected override void OnEnabled()
        {
            listenerEntity.AddListenerSurfaceCreated(this);
        }

        protected override void OnDisabled()
        {
        }

        void IListenerSurfaceCreated.OnSurfaceCreated(SimulationEntityId simulationEntityId)
        {
            var view = CreateGameObject<SurfaceView>(SurfacePrefab, simulationEntityId);
        }

    }
}
