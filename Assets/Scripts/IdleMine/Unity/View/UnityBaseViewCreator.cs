﻿using IdleMine.Utilities;
using UnityEngine;

namespace IdleMine.Unity
{
    public abstract class UnityBaseViewCreator : MonoBehaviour
    {
        protected ListenerEntity listenerEntity;

        protected abstract void OnEnabled();
        protected abstract void OnDisabled();

        private void OnEnable()
        {
            listenerEntity = Contexts.sharedInstance.listener.CreateEntity();
            OnEnabled();
        }

        private void OnDisable()
        {
            if (listenerEntity != null)
            {
                listenerEntity.Destroy();
            }

            OnDisabled();
        }


        protected T CreateGameObject<T>(T prefab, SimulationEntityId simulationEntityId) where T : UnityBaseView
        {
            var view = GameObject.Instantiate(prefab);
            view.Initialize(simulationEntityId);
            return view;
        }


        protected void DestroyGameObject(UnityBaseView view)
        {
            view.Teardown();
            Destroy(view.gameObject);
        }
    }
}
