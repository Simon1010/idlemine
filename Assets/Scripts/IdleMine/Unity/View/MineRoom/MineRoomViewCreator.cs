﻿using IdleMine.Listener;
using IdleMine.Utilities;
using System.Collections.Generic;

namespace IdleMine.Unity
{
    public class MineRoomViewCreator : UnityBaseViewCreator, IListenerAnyMine, IListenerAnyMineRoomExacavated
    {
        public MineRoomExcavatedView MineViewPrefab;
        public MineRoomView MineEmptyPrefab;
        private Dictionary<SimulationEntityId, MineRoomExcavatedView> views;
        private Dictionary<SimulationEntityId, MineRoomView> emptyViews;

        protected override void OnEnabled()
        {
            listenerEntity.AddListenerAnyMine(this);
            listenerEntity.AddListenerAnyMineRoomExcavated(this);
            views = new Dictionary<SimulationEntityId, MineRoomExcavatedView>();
            emptyViews = new Dictionary<SimulationEntityId, MineRoomView>();
        }

        protected override void OnDisabled()
        {
        }

        void IListenerAnyMine.OnMineCreated(SimulationEntityId id)
        {
            if (emptyViews.ContainsKey(id) == false)
            {
                var view = CreateGameObject<MineRoomView>(MineEmptyPrefab, id);
                emptyViews.Add(id, view);
            }
        }

        void IListenerAnyMineRoomExacavated.OnMineRoomExcavated(SimulationEntityId id)
        {
            if (views.ContainsKey(id) == false)
            {
                if (emptyViews.ContainsKey(id))
                {
                    var emptyView = emptyViews[id];
                    emptyView.Teardown();
                    emptyViews.Remove(id);
                }

                var view = CreateGameObject<MineRoomExcavatedView>(MineViewPrefab, id);
                views.Add(id, view);
            }

        }

    }
}
