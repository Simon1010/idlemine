﻿using IdleMine.Utilities;
using System;
using UnityEngine;

namespace IdleMine.Unity
{
    public class MineRoomDataProvider : MonoBehaviour
    {
        [SerializeField] private Transform MineEntrance = null;
        [SerializeField] private Transform Ore = null;
        [SerializeField] private Transform CartStart = null;
        [SerializeField] private Transform CartEnd = null;
        [SerializeField] private Transform MiningPositionContainer = null;
        [SerializeField] private Transform StoringPositionContainer = null;
        [SerializeField] private Transform WaitingPositionContainer = null;


        public System.Numerics.Vector3 GetMineEntrance()
        {
            return MineEntrance.transform.localPosition.ToVector3();
        }

        public System.Numerics.Vector3 GetOrePosition()
        {
            return Ore.transform.localPosition.ToVector3();
        }

        public System.Numerics.Vector3 GetCartStart()
        {
            return CartStart.transform.localPosition.ToVector3();
        }

        public System.Numerics.Vector3 GetCartEnd()
        {
            return CartEnd.transform.localPosition.ToVector3();
        }


        public System.Numerics.Vector3[] GetMiningPoints()
        {
            return GetAllPoints(MiningPositionContainer);
        }

        public System.Numerics.Vector3[] GetWaitingPoints()
        {
            return GetAllPoints(WaitingPositionContainer);
        }
        public System.Numerics.Vector3[] GetStoringPoints()
        {
            return GetAllPoints(StoringPositionContainer);
        }

        private System.Numerics.Vector3[] GetAllPoints(Transform container)
        {
            var points = new System.Numerics.Vector3[container.transform.childCount];
            for (int i = 0; i < container.transform.childCount; i++)
            {
                points[i] = container.transform.GetChild(i).transform.localPosition.ToVector3();
            }
            return points;
        }
    }
}
