﻿using IdleMine.Listener;
using IdleMine.Utilities;
using UnityEngine;

namespace IdleMine.Unity
{
    public class MineRoomView : UnityBaseView, IListenerMineRoomId
    {
        public SpriteRenderer Renderer;

        protected override void OnInitialize()
        {
            CreateView();
            listenerEntity.AddListenerMineRoomId(this);
        }

        protected override void OnTeardown()
        {
        }

        void IListenerMineRoomId.OnMineRoomId(int id)
        {
            var column = MineUtilities.GetMineColumn(id);
            if (column == 0)
            {
                Renderer.flipX = true;
            }
        }

    }
}
