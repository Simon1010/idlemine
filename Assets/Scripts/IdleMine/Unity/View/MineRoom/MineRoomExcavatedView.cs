﻿using UnityEngine;

namespace IdleMine.Unity
{
    public class MineRoomExcavatedView : UnityBaseView
    {
        public Transform EntityContainer;

        internal Transform GetEntityContainer()
        {
            return EntityContainer;
        }

        protected override void OnInitialize()
        {
            CreateView();
        }

        protected override void OnTeardown()
        {
        }

    }
}
