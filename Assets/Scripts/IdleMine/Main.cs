﻿using Entitas;
using IdleMine.Listener;
using IdleMine.Simulation;
using IdleMine.View;
using UnityEngine;
using Vector3 = System.Numerics.Vector3;
using Random = System.Random;
using IdleMine.Unity;

namespace IdleMine
{
    public class Main : MonoBehaviour
    {

        private static Random random;
        public static Random Random
        {
            get
            {
                if (random == null)
                {
                    random = new Random();
                }

                return random;
            }
        }

        private EcsController ecsController;
        private Contexts contexts;

        //TODO : Mine Data
        public MineRoomDataProvider MineDataSource;
        public static MineData TempMineData;
        public class MineData
        {
            public Vector3 MineEntrance;

            public Vector3 CartStart;
            public Vector3 CartEnd;

            public Vector3 OrePosition;
            public Vector3[] MiningPositions;
            public Vector3[] StoringPositions;
            public Vector3[] WaitingPositions;
        }


        private void Awake()
        {
            ecsController = new EcsController();
            contexts = ecsController.Contexts;

            TempMineData = new MineData()
            {
                CartEnd = MineDataSource.GetCartEnd(),
                CartStart = MineDataSource.GetCartStart(),
                MineEntrance = MineDataSource.GetMineEntrance(),
                OrePosition = MineDataSource.GetOrePosition(),
                MiningPositions = MineDataSource.GetMiningPoints(),
                StoringPositions = MineDataSource.GetStoringPoints(),
                WaitingPositions = MineDataSource.GetWaitingPoints(),
            };
        }

        private void Start()
        {
            var systems = new Systems();
            systems.Add(new SimulationFeature(contexts));
            systems.Add(new ListenerFeature(contexts));
            systems.Add(new ViewFeature(contexts));
            systems.Add(new DestroyFeature(contexts));

            ecsController.Initialize(systems);

            contexts.simulation.CreateEntity().isStartGame = true;

            var miner = contexts.simulation.CreateEntity();
            miner.isCharacter = true;
            miner.AddCharacterRole(CharacterRole.Miner);
            miner.AddPosition(new Vector3(-18f, -5f, 0f));
            miner.AddMoveSpeed(0.1f);
            miner.AddMiningCooldown(2f);
            miner.AddMineRoomId(3);
            miner.AddAiTargetMineRoomId(3);
        }

        private void Update()
        {
            ecsController.Update();

            if (Input.GetKeyUp(KeyCode.Space))
            {
                var mineRooms = contexts.simulation.GetEntities(SimulationMatcher.AllOf(SimulationMatcher.MineRoom).NoneOf(SimulationMatcher.MineRoomExcavated));

                if (mineRooms.Length > 0)
                {
                    var randomRoom = mineRooms[Random.Next(0, mineRooms.Length)];
                    randomRoom.isMineRoomExcavated = true;
                }
            }
        }
    }
}
