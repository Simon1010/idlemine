﻿using System.Collections.Generic;
using Entitas;
namespace IdleMine.Listener
{
    public class ListenerMineRoomExcavatedSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public ListenerMineRoomExcavatedSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerAnyMineRoomExcavated);
                foreach (var listenerEntity in listeners)
                {
                        listenerEntity.listenerAnyMineRoomExcavated.value.OnMineRoomExcavated(entity.simulationId.value);
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isMineRoomExcavated;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.MineRoomExcavated);
        }
    }
}
