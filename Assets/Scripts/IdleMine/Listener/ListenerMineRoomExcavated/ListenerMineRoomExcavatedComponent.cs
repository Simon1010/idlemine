﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Listener
{
    [Listener]
    public class ListenerAnyMineRoomExcavatedComponent : IComponent
    {
        public IListenerAnyMineRoomExacavated value;
    }

    public interface IListenerAnyMineRoomExacavated
    {
        void OnMineRoomExcavated(SimulationEntityId id);
    }
}
