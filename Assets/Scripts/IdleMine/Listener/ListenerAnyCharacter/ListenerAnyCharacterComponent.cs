﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Listener
{
    [Listener]
    public class ListenerAnyCharacterComponent : IComponent
    {
        public IListenerAnyCharacter value;
    }

    public interface IListenerAnyCharacter
    {
        void OnAnyCharacterCreated(SimulationEntityId simulationEntityId);
    }
}
