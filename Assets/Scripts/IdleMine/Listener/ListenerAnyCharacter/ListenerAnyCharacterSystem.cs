﻿using System.Collections.Generic;
using Entitas;
namespace IdleMine.Listener
{
    public class ListenerAnyCharacterSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public ListenerAnyCharacterSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerAnyCharacter);
                foreach (var listenerEntity in listeners)
                {
                    listenerEntity.listenerAnyCharacter.value.OnAnyCharacterCreated(entity.simulationId.value);
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isCharacter;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.Character);
        }
    }
}
