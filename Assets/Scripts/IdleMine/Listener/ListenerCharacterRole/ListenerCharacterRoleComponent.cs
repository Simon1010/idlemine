﻿using Entitas;
using IdleMine.Simulation;
using IdleMine.Utilities;

namespace IdleMine.Listener
{
    [Listener]
    public class ListenerCharacterRoleComponent : IComponent
    {
        public IListenerCharacterRole value;
    }

    public interface IListenerCharacterRole
    {
        void OnCharacterRoleChanged(CharacterRole value);
    }
}
