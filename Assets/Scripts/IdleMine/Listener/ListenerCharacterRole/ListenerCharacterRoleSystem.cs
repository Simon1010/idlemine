﻿using System.Collections.Generic;
using Entitas;
namespace IdleMine.Listener
{
    public class ListenerCharacterRoleSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public ListenerCharacterRoleSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerCharacterRole);
                foreach (var listenerEntity in listeners)
                {
                    if (listenerEntity.hasListenerTarget && listenerEntity.listenerTarget.value == entity.simulationId.value)
                    {
                        listenerEntity.listenerCharacterRole.value.OnCharacterRoleChanged(entity.characterRole.value);
                    }
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.hasCharacterRole;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.CharacterRole);
        }
    }
}
