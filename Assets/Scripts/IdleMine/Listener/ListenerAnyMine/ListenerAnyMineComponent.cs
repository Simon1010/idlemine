﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Listener
{
    [Listener]
    public class ListenerAnyMineComponent : IComponent
    {
        public IListenerAnyMine value;
    }

    public interface IListenerAnyMine
    {
        void OnMineCreated(SimulationEntityId id);
    }
}
