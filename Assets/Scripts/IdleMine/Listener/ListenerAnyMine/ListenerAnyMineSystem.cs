﻿using System.Collections.Generic;
using Entitas;
namespace IdleMine.Listener
{
    public class ListenerAnyMineSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public ListenerAnyMineSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerAnyMine);
                foreach (var listenerEntity in listeners)
                {
                    listenerEntity.listenerAnyMine.value.OnMineCreated(entity.simulationId.value);
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isMineRoom;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.MineRoom);
        }
    }
}
