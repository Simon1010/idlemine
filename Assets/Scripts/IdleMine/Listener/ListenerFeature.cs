﻿using Entitas;

namespace IdleMine.Listener
{
    public class ListenerFeature : Feature
    {
        public ListenerFeature(Contexts contexts)
        {
            Add(new ListenerAnyMineSystem(contexts));
            Add(new ListenerAnyOreCreatedSystem(contexts));
            Add(new ListenerAnyCartSystem(contexts));
            Add(new ListenerAnyCharacterSystem(contexts));
            Add(new ListenerCharacterRoleSystem(contexts));
            Add(new ListenerMineRoomExcavatedSystem(contexts));
            Add(new ListenerMineRoomIdSystem(contexts));
            Add(new ListenerDestroyedSystem(contexts));
            Add(new ListenerSurfaceCreatedSystem(contexts));
            Add(new EventListenerMineRoomIdSystem(contexts));
            Add(new ListenerPositionSystem(contexts));
            Add(new EventListenerPositionSystem(contexts));
        }
    }
}
