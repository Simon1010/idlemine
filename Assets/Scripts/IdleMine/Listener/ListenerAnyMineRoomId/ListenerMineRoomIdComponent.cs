﻿
using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Listener
{
    [Listener]
    public class ListenerAnyMineRoomIdComponent : IComponent
    {
        public IListenerAnyMineRoomId value;
    }

    public interface IListenerAnyMineRoomId
    {
        void OnAnyMineRoomId(SimulationEntityId id, int mineRoomId);
    }
}
