﻿using System.Collections.Generic;
using Entitas;
namespace IdleMine.Listener
{
    public class EventAnyListenerMineRoomIdSystem : ReactiveSystem<ListenerEntity>
    {
        private readonly Contexts contexts;

        public EventAnyListenerMineRoomIdSystem(Contexts contexts) : base(contexts.listener)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<ListenerEntity> entities)
        {
            foreach (var listenerEntity in entities)
            {
                var target = contexts.simulation.GetEntityWithSimulationId(listenerEntity.listenerTarget.value);
                listenerEntity.listenerAnyMineRoomId.value.OnAnyMineRoomId(target.simulationId.value, target.mineRoomId.value);
            }
        }

        protected override bool Filter(ListenerEntity entity)
        {
            return entity.hasListenerAnyMineRoomId;
        }

        protected override ICollector<ListenerEntity> GetTrigger(IContext<ListenerEntity> context)
        {
            return context.CreateCollector(ListenerMatcher.AllOf(ListenerMatcher.ListenerAnyMineRoomId, ListenerMatcher.TriggerOnAdd));
        }
    }
}
