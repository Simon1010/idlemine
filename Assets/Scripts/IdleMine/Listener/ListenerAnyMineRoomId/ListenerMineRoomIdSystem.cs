﻿using System.Collections.Generic;
using Entitas;
namespace IdleMine.Listener
{
    public class ListenerAnyMineRoomIdSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public ListenerAnyMineRoomIdSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerAnyMineRoomId);
                foreach (var listenerEntity in listeners)
                {
                    listenerEntity.listenerAnyMineRoomId.value.OnAnyMineRoomId(entity.simulationId.value, entity.mineRoomId.value);
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.hasMineRoomId;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.MineRoomId);
        }
    }
}
