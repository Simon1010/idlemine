﻿using System.Collections.Generic;
using Entitas;
namespace IdleMine.Listener
{
    public class ListenerMineRoomIdSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public ListenerMineRoomIdSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerMineRoomId);
                foreach (var listenerEntity in listeners)
                {
                    if (listenerEntity.listenerTarget.value == entity.simulationId.value)
                    {
                        listenerEntity.listenerMineRoomId.value.OnMineRoomId(entity.mineRoomId.value);
                    }
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.hasMineRoomId;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.MineRoomId);
        }
    }
}
