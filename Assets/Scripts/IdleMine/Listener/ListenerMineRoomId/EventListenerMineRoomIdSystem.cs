﻿using System.Collections.Generic;
using Entitas;
namespace IdleMine.Listener
{
    public class EventListenerMineRoomIdSystem : ReactiveSystem<ListenerEntity>
    {
        private readonly Contexts contexts;

        public EventListenerMineRoomIdSystem(Contexts contexts) : base(contexts.listener)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<ListenerEntity> entities)
        {
            foreach (var listenerEntity in entities)
            {
                var target = contexts.simulation.GetEntityWithSimulationId(listenerEntity.listenerTarget.value);
                listenerEntity.listenerMineRoomId.value.OnMineRoomId(target.mineRoomId.value);
            }
        }

        protected override bool Filter(ListenerEntity entity)
        {
            return entity.hasListenerMineRoomId;
        }

        protected override ICollector<ListenerEntity> GetTrigger(IContext<ListenerEntity> context)
        {
            return context.CreateCollector(ListenerMatcher.AllOf(ListenerMatcher.ListenerMineRoomId, ListenerMatcher.TriggerOnAdd));
        }
    }
}
