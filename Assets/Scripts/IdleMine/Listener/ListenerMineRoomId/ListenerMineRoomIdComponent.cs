﻿
using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Listener
{
    [Listener]
    public class ListenerMineRoomIdComponent : IComponent
    {
        public IListenerMineRoomId value;
    }

    public interface IListenerMineRoomId
    {
        void OnMineRoomId(int id);
    }
}
