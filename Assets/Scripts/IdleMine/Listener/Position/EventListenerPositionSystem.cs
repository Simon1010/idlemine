﻿using System.Collections.Generic;
using Entitas;
namespace IdleMine.Listener
{
    public class EventListenerPositionSystem : ReactiveSystem<ListenerEntity>
    {
        private readonly Contexts contexts;

        public EventListenerPositionSystem(Contexts contexts) : base(contexts.listener)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<ListenerEntity> entities)
        {
            foreach (var listenerEntity in entities)
            {
                var target = contexts.simulation.GetEntityWithSimulationId(listenerEntity.listenerTarget.value);
                if(target.hasPosition != false)
                listenerEntity.listenerPosition.value.OnPosition(target.position.value);
            }
        }

        protected override bool Filter(ListenerEntity entity)
        {
            return entity.hasListenerPosition;
        }

        protected override ICollector<ListenerEntity> GetTrigger(IContext<ListenerEntity> context)
        {
            return context.CreateCollector(ListenerMatcher.AllOf(ListenerMatcher.ListenerPosition, ListenerMatcher.TriggerOnAdd));
        }
    }
}
