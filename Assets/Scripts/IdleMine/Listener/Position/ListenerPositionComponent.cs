﻿using Entitas;
using System.Numerics;

namespace IdleMine.Listener
{
    [Listener]
    public class ListenerPositionComponent : IComponent
    {
        public IListenerPosition value;
    }

    public interface IListenerPosition
    {
        void OnPosition(Vector3 value);
    }
}
