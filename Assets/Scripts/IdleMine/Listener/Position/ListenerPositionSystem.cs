﻿using System.Collections.Generic;
using Entitas;
namespace IdleMine.Listener
{
    public class ListenerPositionSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public ListenerPositionSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerPosition);
                foreach (var listenerEntity in listeners)
                {
                    if (listenerEntity.listenerTarget.value == entity.simulationId.value)
                    {
                        listenerEntity.listenerPosition.value.OnPosition(entity.position.value);
                    }
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.hasPosition;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.Position);
        }
    }
}
