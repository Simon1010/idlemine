﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Listener
{
    [Listener]
    public class ListenerAnyCartComponent : IComponent
    {
        public IListenerAnyCart value;
    }

    public interface IListenerAnyCart
    {
        void OnCartCreated(SimulationEntityId id);
    }
}
