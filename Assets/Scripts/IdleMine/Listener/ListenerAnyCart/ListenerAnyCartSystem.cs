﻿using System.Collections.Generic;
using Entitas;
namespace IdleMine.Listener
{
    public class ListenerAnyCartSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public ListenerAnyCartSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerAnyCart);
                foreach (var listenerEntity in listeners)
                {
                    listenerEntity.listenerAnyCart.value.OnCartCreated(entity.simulationId.value);
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isCart;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.Cart);
        }
    }
}
