﻿using System.Collections.Generic;
using Entitas;

namespace IdleMine.Listener
{
    public class ListenerSurfaceCreatedSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public ListenerSurfaceCreatedSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerSurfaceCreated);
                foreach (var listener in listeners)
                {
                    listener.listenerSurfaceCreated.value.OnSurfaceCreated(entity.simulationId.value);
                }
            }
        }


        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isSurface;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.Surface);
        }
    }
}
