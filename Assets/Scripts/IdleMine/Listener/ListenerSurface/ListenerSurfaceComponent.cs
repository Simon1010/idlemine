﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Listener
{
    [Listener]
    public class ListenerSurfaceCreatedComponent : IComponent
    {
        public IListenerSurfaceCreated value;
    }

    public interface IListenerSurfaceCreated
    {
        void OnSurfaceCreated(SimulationEntityId simulationEntityId);
    }
}
