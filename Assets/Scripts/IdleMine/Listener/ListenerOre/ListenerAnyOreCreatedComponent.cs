﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Listener
{
    [Listener]
    public class ListenerAnyOreCreatedComponent : IComponent
    {
        public IListenerAnyOreCreated value;
    }

    public interface IListenerAnyOreCreated
    {
        void OnAnyOreCreated(SimulationEntityId simulationEntityId);
    }
}
