﻿using System.Collections.Generic;
using Entitas;

namespace IdleMine.Listener
{
    public class ListenerAnyOreCreatedSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public ListenerAnyOreCreatedSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerAnyOreCreated);
                foreach (var listener in listeners)
                {
                    listener.listenerAnyOreCreated.value.OnAnyOreCreated(entity.simulationId.value);
                }
            }
        }


        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isOre;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.Ore);
        }
    }
}
