﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Listener
{
    [Listener]
    public class ListenerTargetComponent : IComponent
    {
        public SimulationEntityId value;
    }
}
