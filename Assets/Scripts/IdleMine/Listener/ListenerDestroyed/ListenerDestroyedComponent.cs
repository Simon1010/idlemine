﻿using Entitas;
using IdleMine.Utilities;

namespace IdleMine.Listener
{
    [Listener]
    public class ListenerDestroyedComponent : IComponent
    {
        public IListenerDestroyed value;
    }

    public interface IListenerDestroyed
    {
        void OnDestroyed();
    }
}
