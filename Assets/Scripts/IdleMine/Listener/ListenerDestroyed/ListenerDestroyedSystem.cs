﻿using System.Collections.Generic;
using Entitas;
namespace IdleMine.Listener
{
    public class ListenerDestroyedSystem : ReactiveSystem<SimulationEntity>
    {
        private readonly Contexts contexts;

        public ListenerDestroyedSystem(Contexts contexts) : base(contexts.simulation)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<SimulationEntity> entities)
        {
            foreach (var entity in entities)
            {
                var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerDestroyed);
                foreach (var listenerEntity in listeners)
                {
                    if (listenerEntity.hasListenerTarget && listenerEntity.listenerTarget.value == entity.simulationId.value)
                        listenerEntity.listenerDestroyed.value.OnDestroyed();
                }
            }
        }

        protected override bool Filter(SimulationEntity entity)
        {
            return entity.isDestroyed;
        }

        protected override ICollector<SimulationEntity> GetTrigger(IContext<SimulationEntity> context)
        {
            return context.CreateCollector(SimulationMatcher.Destroyed);
        }
    }
}
